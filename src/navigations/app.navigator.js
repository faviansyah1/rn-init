// import 'react-native-gesture-handler';
import React from 'react';
import {DefaultTheme, NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import styles from 'assets/styles';
import {Icon} from '@ui-kitten/components';
import {useTheme} from '@ui-kitten/components';
console.disableYellowBox = true;
/*
 * Navigation theming: https://reactnavigation.org/docs/en/next/themes.html
 */
import HomeStack from './home.navigator';
import ProfileComponent from 'screens/Profile';
import CartComponent from 'screens/Cart';

const navigatorTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    // prevent layout blinking when performing navigation
    background: 'transparent',
  },
};

const tabBarOptions = {
  labelStyle: {
    fontSize: 12,
    paddingTop: 0,
    fontFamily: 'Gotham-Medium',
  },
  style: {
    paddingBottom: 10,
    paddingTop: 10,
    height: 65,
    borderTopWidth: 0,
    elevation: 15,
    paddingHorizontal: 15,
  },
};

const Tab = createBottomTabNavigator();
export const AppNavigator = () => {
  const theme = useTheme();
  return (
    <NavigationContainer theme={navigatorTheme}>
      <Tab.Navigator
        tabBarOptions={{
          ...tabBarOptions,
          activeTintColor: theme['color-primary-500'],
          inactiveTintColor: theme['color-primary-300'],
        }}>
        <Tab.Screen
          name="Home"
          component={HomeStack}
          options={{
            tabBarLabel: 'Explore',
            tabBarIcon: ({color}) => (
              <Icon
                style={styles.extraSmall}
                fill={color}
                width={27}
                name="globe-2"
              />
            ),
          }}
        />
        <Tab.Screen
          name="Cart"
          component={CartComponent}
          options={{
            tabBarLabel: 'Cart',
            tabBarIcon: ({color}) => (
              <Icon
                style={styles.extraSmall}
                fill={color}
                width={27}
                name="shopping-cart"
              />
            ),
          }}
        />
        <Tab.Screen
          name="Settings"
          component={ProfileComponent}
          options={{
            tabBarLabel: 'Me',
            tabBarIcon: ({color}) => (
              <Icon
                style={styles.extraSmall}
                width={27}
                fill={color}
                name="person"
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
