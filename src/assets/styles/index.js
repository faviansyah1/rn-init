const SPACE = 15;
import {StyleSheet} from 'react-native';
import {width} from 'constants/dimensions';
module.exports = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#f9f9f9',
  },
  flex: {
    flex: 1,
  },
  grow: {
    flexGrow: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  column: {
    flexDirection: 'column',
  },
  row: {
    flexDirection: 'row',
  },
  cover: {
    width: width / 1.4,
  },
  container: {
    paddingHorizontal: SPACE,
  },
  containercenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 15,
    paddingRight: 15,
  },
  shadow: {
    elevation: 5,
  },
  scrollview: {
    paddingBottom: 50,
  },

  //image
  extraSmall: {
    width: 32,
    height: 32,
  },

  //align
  stretch: {
    alignSelf: 'stretch',
  },
  flexWrap: {
    flexWrap: 'wrap',
  },
  justifyleft: {
    justifyContent: 'flex-start',
  },
  justifycenter: {
    justifyContent: 'center',
  },
  justifyright: {
    justifyContent: 'flex-end',
  },
  justifybetween: {
    justifyContent: 'space-between',
  },
  alignstart: {
    alignSelf: 'flex-start',
  },
  aligncenter: {
    alignItems: 'center',
  },
  alignright: {
    flex: 1,
    alignItems: 'flex-end',
  },
  left: {
    alignItems: 'flex-start',
  },
  right: {
    alignItems: 'flex-end',
  },
  textcenter: {
    textAlign: 'center',
  },
  textleft: {
    textAlign: 'left',
  },
  textright: {
    textAlign: 'right',
  },
  underline: {
    textDecorationLine: 'underline',
  },
  //space
  mt0: {marginTop: 0},
  mt5: {marginTop: 5},
  mt10: {marginTop: 10},
  mt15: {marginTop: 15},
  mt20: {marginTop: 20},
  mt25: {marginTop: 25},
  mt30: {marginTop: 30},
  mt50: {marginTop: 50},
  mb0: {marginBottom: 0},
  mb5: {marginBottom: 5},
  mb10: {marginBottom: 10},
  mb15: {marginBottom: 15},
  mb20: {marginBottom: 20},
  mb25: {marginBottom: 25},
  mb30: {marginBottom: 30},
  mr1: {marginRight: 1},
  mr5: {marginRight: 5},
  mr10: {marginRight: 10},
  mr15: {marginRight: 15},
  mr20: {marginRight: 20},
  ml5: {marginLeft: 5},
  ml10: {marginLeft: 10},
  ml15: {marginLeft: 15},
  ml20: {marginLeft: 20},

  pt5: {paddingTop: 5},
  pt10: {paddingTop: 10},
  pt15: {paddingTop: 15},
  pt20: {paddingTop: 20},
  pt25: {paddingTop: 25},
  pt30: {paddingTop: 30},
  pb5: {paddingBottom: 5},
  pb10: {paddingBottom: 10},
  pb15: {paddingBottom: 15},
  pb20: {paddingBottom: 20},
  pb25: {paddingBottom: 25},
  pb30: {paddingBottom: 30},
  pr5: {paddingRight: 5},
  pr10: {paddingRight: 10},
  pr15: {paddingRight: 15},
  pr20: {paddingRight: 20},
  pl5: {paddingLeft: 5},
  pl10: {paddingLeft: 10},
  pl15: {paddingLeft: 15},
  pl20: {paddingLeft: 20},

  ph5: {paddingHorizontal: 5},
  ph10: {paddingHorizontal: 10},
  ph15: {paddingHorizontal: 15},
  ph20: {paddingHorizontal: 20},
  ph25: {paddingHorizontal: 25},
  ph30: {paddingHorizontal: 30},
  ph35: {paddingHorizontal: 35},
  ph40: {paddingHorizontal: 40},
  pv5: {paddingVertical: 5},
  pv8: {paddingVertical: 8},
  pv10: {paddingVertical: 10},
  pv11: {paddingVertical: 11},
  pv15: {paddingVertical: 15},
  pv20: {paddingVertical: 20},
  pv25: {paddingVertical: 25},
  pv30: {paddingVertical: 30},

  lh15: {lineHeight: 15},
  lh20: {lineHeight: 20},
  lh21: {lineHeight: 21},
  lh22: {lineHeight: 22},
  lh23: {lineHeight: 23},
  lh24: {lineHeight: 24},
  lh25: {lineHeight: 25},
  lh26: {lineHeight: 26},
  lh27: {lineHeight: 27},
  lh28: {lineHeight: 28},
  lh29: {lineHeight: 29},
  lh30: {lineHeight: 30},
  lh35: {lineHeight: 35},
});
