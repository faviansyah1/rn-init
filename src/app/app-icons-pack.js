import React from 'react';
import {Image} from 'react-native';

const IconProvider = (source) => ({
  toReactElement: ({animation, ...style}) => (
    <Image style={style} source={source} />
  ),
});

export const AppIconsPack = {
  name: 'app',
  icons: {
    auth: IconProvider(require('assets/icons/star.png')),
  },
};
