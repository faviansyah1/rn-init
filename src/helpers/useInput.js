import {useState, useCallback} from 'react';

function useInput(initialValue) {
  const [value, setValue] = useState(initialValue);
  const reset = () => {
    setValue(initialValue);
  };
  const handleChange = useCallback(e => {
    setValue(e);
  }, []);
  const bind = {
    value,
    onChange: handleChange,
  };

  return [value, bind, reset];
}

export default useInput;
