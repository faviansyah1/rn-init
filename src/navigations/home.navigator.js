import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeComponent from 'screens/Home';

const Stack = createStackNavigator();

export default function MyStack() {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={HomeComponent} />
    </Stack.Navigator>
  );
}
