import moment from 'moment';
export function dateFormat(time) {
  var today = moment().format('DD-MMM-YY');
  var yesterday = moment()
    .add(-1, 'days')
    .format('DD-MMM-YY');
  if (today == time) {
    return 'Hari ini';
  } else if (yesterday == time) {
    return 'Kemarin';
  } else {
    return moment(time, 'DD-MMM-YY').format('DD MMM YYYY');
  }
}

export function dateFormatPass(time) {
  var timeNow = moment(time, 'YYYY-MM-DDTHH:mm:ssZ').format('DD-MMM-YY');
  var today = moment().format('DD-MMM-YY');
  var yesterday = moment()
    .add(-1, 'days')
    .format('DD-MMM-YY');
  if (today == timeNow) {
    return moment(time, 'YYYY-MM-DDTHH:mm:ssZ').format('HH:mm');
  } else if (yesterday == timeNow) {
    return 'Kemarin';
  } else {
    return moment(time, 'YYYY-MM-DDTHH:mm:ssZ').format('DD MMM YYYY');
  }
}

export function parseDate(date, format) {
  return moment(date).format(format);
}
