import AsyncStorage from '@react-native-community/async-storage';
import {APP_KEY, APP_KEY_USER} from 'react-native-dotenv';
export const storage = {
  sendToken,
  getToken,
  removeToken,
  sendUser,
  getUser,
};

function sendToken(token) {
  AsyncStorage.setItem(APP_KEY, token);
}

function sendUser(user) {
  AsyncStorage.setItem(APP_KEY_USER, user);
}

async function getUser() {
  try {
    const dataUser = await AsyncStorage.getItem(APP_KEY_USER);
    return JSON.parse(dataUser);
  } catch (e) {
    // logoutHandler();
    return false;
  }
}

function getToken() {
  try {
    return AsyncStorage.getItem(APP_KEY);
  } catch (e) {
    // logoutHandler();
    return false;
  }
}

function removeToken() {
  AsyncStorage.removeItem(APP_KEY);
}
