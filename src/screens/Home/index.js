import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';

export default function Home() {
  return (
    <View style={styles.container}>
      <Text style={styles.about}>Welcome To React Native Template</Text>
    </View>
  );
}
