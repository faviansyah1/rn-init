import {types} from 'constants/auth';

const initialState = {
  auth: {
    data: {},
    loading: false,
    loadingVerify: false,
    errorCode: null,
    errorMessage: '',
  },
};

const auth = (state = initialState.auth, action) => {
  switch (action.type) {
    case types.AUTH_LOADING:
      return {
        ...state,
        loading: true,
      };
    case types.AUTH_SUCCESS:
      return {
        ...state,
        loadingVerify: false,
        errorCode: null,
        errorMessage: '',
        data: action.data,
      };
    case types.AUTH_FAILED:
      return {
        ...state,
        loading: false,
        loadingVerify: false,
        errorCode: action.code,
        errorMessage: action.message,
        data: {},
      };
    case types.AUTH_RESET:
      return {
        ...state,
        errorCode: null,
        errorMessage: '',
        data: {},
      };
    default:
      return state;
  }
};
export default auth;
