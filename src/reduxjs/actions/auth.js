import {types, URL_LOGIN} from 'constants/auth';
import api from 'helpers/api';
import {showMessage} from 'react-native-flash-message';
import {ERRORSTYLE} from 'helpers/alert';

export function requestLogin() {
  return async (dispatch) => {
    dispatch({type: types.AUTH_LOADING});
    api
      .get(URL_LOGIN)
      .then((response) => {
        const {data} = response.data;
        dispatch({type: types.AUTH_SUCCESS, data});
      })
      .catch((error) => {
        showMessage({message: error.message || error.desc, ...ERRORSTYLE});
        dispatch({
          type: types.AUTH_FAILED,
          code: error.code,
          message: error.message,
        });
      });
  };
}
