export const URL_LOGIN = '/account/sign-in';

export const types = {
  AUTH_LOADING: 'AUTH_LOADING',
  AUTH_SUCCESS: 'AUTH_SUCCESS',
  AUTH_FAILED: 'AUTH_FAILED',
  AUTH_RESET: 'AUTH_RESET',
};
